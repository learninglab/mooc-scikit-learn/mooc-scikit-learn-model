# Mooc Scikit-Learn Model

This repository is used to initialized learners environment by downloading notebooks and datasets from https://github.com/INRIA/scikit-learn-mooc. This is used to et a reset url and to filter out solution notebooks.

To manually update (using javascript console):

1. Open a javascript console (F12 o Ctrl + Shift + I)
2. Copy and paste the following script
3. Copy the json output
4. Paste it inside `notebooks.json`

```js
const baseUrl = "https://github.com/INRIA/scikit-learn-mooc/raw/main/";
const apiUrl = "https://api.github.com/repos/INRIA/scikit-learn-mooc/contents";
let datasets, notebooks, wrapups, figures;

fetch(`${apiUrl}/datasets`)
.then(function(response_datasets) {
    return response_datasets.json()
}).then(function(data_datasets) {
    datasets = data_datasets.map(item => item.path)
    return fetch(`${apiUrl}/figures`)
}).then(function(response_figures) {
    return response_figures.json()
}).then(function(data_figures) {
    figures = data_figures.map(item => item.path)
    return fetch(`${apiUrl}/notebooks`)
}).then(function(response_notebooks) {
    return response_notebooks.json()
}).then(function(data_notebooks) {
    // Filter out solutions
    notebooks = data_notebooks.map(item => item.path).filter(item => item.indexOf('sol') === -1)
    wrapups = [
        "evaluation/ensemble_questions.ipynb",
        "evaluation/linear_models_questions.ipynb",
        "evaluation/trees_questions.ipynb",
        "evaluation/predictive_modeling_pipeline_questions.ipynb",
        "evaluation/overfit_questions.ipynb",
        "evaluation/tuning_questions.ipynb",
        "evaluation/evaluation_questions.ipynb",
        "evaluation/sandbox_notebook.ipynb"
    ];
    console.log(JSON.stringify({baseUrl, notebooks, datasets, wrapups, figures}, null, 2))
});
```
